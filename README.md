# Grab AI Challenge - Traffic Management

## Problem Statement

Economies in Southeast Asia are turning to AI to solve traffic congestion, which hinders mobility and economic growth. The first step in the push towards alleviating traffic congestion is to understand travel demand and travel patterns within the city. Can we accurately forecast travel demand based on historical Grab bookings to predict areas and times with high travel demand?

## Problem Analysis

In online transportation domain, one of the most important things is availability. For every demand that comes from customer, there must be available driver. To achieve its availability, one of the common way is by predicting the demand for every area at a given time. This prediction is beneficial for driver and customer too. The system can recomend the driver to go to the high-demand area at busy time, so, at busy time when demand are high, there's still enough driver to fulfill the order. The driver can fulfill the demand at the shorter time and the price too will decrease. For customer, the system can recommend customer to use GrabShare at busy time in their area. Why GrabShare? Imagine if at 17.00 p.m, A, B, and C from near location use Grab to the same mall, they will order three different GrabCar, this will increasing the traffic, the cost, and polution. A, B, and C doesn't want to use GrabShare, because maybe they don't want to spend more time if the driver have to pick passenger from far area (because the driver gonna pick all the passengers and then drop-off the passengers). But imagine, if the system can give they a recommendation, like 'In 17.00 p.m there's a high demand in your area, why don't use GrabShare? Cheaper and you save the world by reducing traffic jam and polution'. Happy customer, happy driver.

The question is, how to predict the demand at a given time in a certain location, accurately?

## Data Analysis

Based on analysis, there are several interesting finding:

1. In this problem, there are no significant behavior differences between one location to other, but there is a huge demand difference between one location to other. There are several area which have high demand, medium deman, and low demand.

2. Day is significantly influence the demand, because several days have a different demand pattern.

3. On weekly basis, there are similar patter for every 7 days.

## Model

I use Random Forest Regressor with day and demand based modeling. There are two big parts in the `traffic_management.ipynb` file, which are **Testing** and **Exploration**. To create the model, predict the data and see model performaces, you can just see the **Testing** part. But, if you want to see the analysis behind the idea of this solution, please kindly see the **Exploration** part.


## Future Improvements

Even though the model can learn better by using day and demand based modeling, I guess it's better to add more features like public holiday data and weather forecast data. The model can perform well in data that have similar patter, but if the model used to predict data that have different pattern because of external causes, I'm not sure that it will still perform well. 

On this challenge, I choose to use the notebook, because in my point of view, notebook is more suitable for data exploration. I want to show the step by step analysis that I have done, the idea behind my solution and of course my solution. But for future improvements and production, it's highly recommended to not use notebook.



